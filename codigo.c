#include <stdio.h>
int main(int argc, char *argv[]) {
  printf("Programa para saber tamaños de bytes de las siguientes 
variables\n");
  printf("Un Entero(Int) ocupa:%lu bytes en memoria\n",sizeof(int));
  printf("Un Float ocupa:%lu bytes en memoria\n",sizeof(float));
  printf("Un Char ocupa:%lu bytes en memoria\n",sizeof(char));
  printf("Un Entero largo (Long Int) ocupa:%lu bytes en 
memoria\n",sizeof(long int));
  return 0;
}

